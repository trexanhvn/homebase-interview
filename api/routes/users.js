var express = require('express');
const users = require('../models/users');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/:id', async function(req, res, next) {
  const userInfo = await users.findById(req.params.id);
  res.json(userInfo);
  // res.send('get user id');
});

router.put('/:id', async function(req, res, next) {
  const id = parseInt(req.params.id);
  const userInfo = await users.findById(id);
  if (userInfo) {
    const {firstName, lastName, email, phone} = req.body;
    await users.update({firstName, lastName, email, phone});
  } else {
    res.send('User not found')
  }
  res.send('update user info by id');
});

router.post('/', async function(req, res, next) {
  const {firstName, lastName, email, phone} = req.body;
  try {
    const user = await users.create({firstName, lastName, email, phone});
  } catch (err) {
    res.send(err);
  }
  res.send('create user successfully');
});

router.delete('/:id', async function(req, res, next) {
  const id = parseInt(req.params.id);
  const userInfo = await users.findById(id);
  if (userInfo) {
    await userInfo.destroy();
    return res.send('Deleted');
  } else {
    return res.send('User not found');
  }
  res.send('delete user info');
});
module.exports = router;
